ifeq ($(strip $(WITH_GMS)),true)

ifeq ($(strip $(TARGET_CORE_GMS)),true)
PRODUCT_PACKAGES += \
   GoogleCoreConfigOverlay
    
else
PRODUCT_PACKAGES += \
    PixelBuiltInPrintService \
    CellBroadcastReceiverOverlay \
    CellBroadcastServiceOverlay \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GoogleWebViewOverlay \
    ManagedProvisioningPixelOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelConfigOverlayCommon \
    PixelConnectivityOverlay2022 \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizard_rro \
    PixelTetheringOverlay2021 \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    Pixelframework-res

endif

ifeq ($(strip $(TARGET_USE_GOOGLE_TELEPHONY)),true)
PRODUCT_PACKAGES += \
    GmsConfigOverlayComms \
    GmsConfigOverlayTelecom \
    GmsConfigOverlayTeleService \
    GmsConfigOverlayTelephony \
    PixelTeleService \
    PixelTelecom \
    GoogleDialerOverlay
endif

endif
